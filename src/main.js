import Vue from 'vue';
import 'document-register-element/build/document-register-element';
import vueCustomElement from 'vue-custom-element';
import AutocompleteTextfield from './components/AutocompleteTextfield';

// use this flag to debug your custom element
// Vue.config.devtools = true;

Vue.use(vueCustomElement);
Vue.customElement('autocomplete-textfield', AutocompleteTextfield);

// new Vue({
//   el: '#app',
//   components: {
//     AutocompleteTextfield
//   },
//   template: `<autocomplete-textfield></autocomplete-textfield>`
// });


